import java.io.*;
import java.util.*;

public record Player(String alias,int yearLastMatch,int monthLastMatch , int dayLastMatch, int scorePerMinute, int wins, int looses) implements Comparable<Player> {

    public static void main(String[] args) throws IOException {

        findPlayer("src/main/resources/playerdata.bin","HunterKiller11111elf", 15000,1337 , 0);
        findPlayer("src/main/resources/playerdata.bin","CyberBob", 2, 1  ,  354);
        findPlayer("src/main/resources/playerdata.bin","ShadowDeath42", 3 ,0 , 400);


        System.out.println(read("src/main/resources/playerdata.bin"));

    }

    public static Player createPlayer(byte[] bytearr) throws IOException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytearr);
        DataInputStream dis = new DataInputStream(bis);

        String alias = dis.readUTF();

        int yearLastMatch = dis.readInt();

        int monthLastMatch = dis.readInt();

        int dayLastMatch = dis.readInt();

        int scorePerMinute = dis.readInt();

        int wins = dis.readInt();

        int looses = dis.readInt();

        dis.close();
        bis.close();

        return new Player(alias, yearLastMatch, monthLastMatch, dayLastMatch, scorePerMinute, wins, looses);
    }

    public static List<Player> read(String file) throws IOException {
        List<Player> playerList = new ArrayList<>();

        try (DataInputStream dis = new DataInputStream(new FileInputStream(file))) {

            dis.skipBytes(1345);
            short playerCount=dis.readShort();

                for (int i = 0; i < playerCount; i++) {
                    byte[] playerData = new byte[dis.readShort()+2];
                    dis.read(playerData);
                    Player player = createPlayer(playerData);
                    playerList.add(player);
                    Collections.sort(playerList);
                }



        } catch (FileNotFoundException e) {
            System.err.println(e + " : " + file);
        }
        return playerList;
    }


    public static void findPlayer(String filename, String alias, int newScorePerMinute, int newWins, int newLosses) {
        try (RandomAccessFile raf = new RandomAccessFile(filename, "rw")) {
            raf.skipBytes(1345);
            short playercount = raf.readShort();
            try {
                for (int i = 0; i < playercount; i++) {
                    raf.readShort();
                    String name = raf.readUTF();
                    if (name.equals(alias)) {
                        raf.skipBytes(12);
                        raf.writeInt(newScorePerMinute);
                        raf.writeInt(newWins);
                        raf.writeInt(newLosses);
                        return;
                    } else {
                        raf.skipBytes(24);
                    }
                }
                throw new IllegalArgumentException("Fehler");

            } catch (FileNotFoundException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public int compareTo(Player that) {
        if (this.scorePerMinute != that.scorePerMinute) {
            return (this.scorePerMinute > that.scorePerMinute ? -1 : 1);
        }

        if (this.alias == null && that.alias == null) {
            // pass
        } else if (this.alias == null) {
            return -1;
        } else if (that.alias == null) {
            return 1;
        } else {
            int aliasComparison = this.alias.compareTo(that.alias);
            if (aliasComparison != 0) {
                return aliasComparison < 0 ? -1 : 1;
            }
        }

        if (this.yearLastMatch != that.yearLastMatch) {
            return (this.yearLastMatch < that.yearLastMatch ? -1 : 1);
        }

        if (this.monthLastMatch != that.monthLastMatch) {
            return (this.monthLastMatch < that.monthLastMatch ? -1 : 1);
        }

        if (this.dayLastMatch != that.dayLastMatch) {
            return (this.dayLastMatch < that.dayLastMatch ? -1 : 1);
        }

        if (this.wins != that.wins) {
            return (this.wins < that.wins ? -1 : 1);
        }

        if (this.looses != that.looses) {
            return (this.looses < that.looses ? -1 : 1);
        }

        return 0;
    }
}
